package ca.alexsoares.nepastoucherlescaseblanche;

/**
 * Created by Alex on 12/11/2017.
 */

public class Casse {
    private int verticale;
    private int horizontal;
    private int longueur;
    private int hauteur;

    public Casse(int verticale, int horizontal, int longueur, int hauteur) {
        this.verticale = verticale;
        this.horizontal = horizontal;
        this.longueur = longueur;
        this.hauteur = hauteur;
    }

    public int getLongueur() {
        return longueur;
    }

    public void setLongueur(int longueur) {
        this.longueur = longueur;
    }

    public int getHauteur() {
        return hauteur;
    }

    public void setHauteur(int hauteur) {
        this.hauteur = hauteur;
    }

    public int getVerticale() {
        return verticale;
    }

    public void setVerticale(int verticale) {
        this.verticale = verticale;
    }

    public int getHorizontal() {
        return horizontal;
    }

    public void setHorizontal(int horizontal) {
        this.horizontal = horizontal;
    }
}
