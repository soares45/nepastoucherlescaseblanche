package ca.alexsoares.nepastoucherlescaseblanche;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Chronometer;

public class HolderGridLineView extends AppCompatActivity {

    public static GridLineView jeu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_holder_grid_line_view);
        jeu = (GridLineView) findViewById(R.id.GridLineView);
        jeu.setChronometer((Chronometer) findViewById(R.id.chronometer));

    }
}
