package ca.alexsoares.nepastoucherlescaseblanche;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Chronometer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 12/10/2017.
 */

public class GridLineView extends View{
    private static final int DEFAULT_PAINT_COLOR_LIGNE = Color.BLACK;
    private static final int DEFAULT_PAINT_COLOR_RECT_TOUCH = Color.RED;
    private static final int DEFAULT_PAINT_COLOR_RECT = Color.BLACK;
    private static final int DEFAULT_PAINT_COLOR_RECT_VICTOIRE = Color.GREEN;
    private static final int DEFAULT_NUMBER_OF_ROWS = 4;
    private static final int DEFAULT_NUMBER_OF_COLUMNS = 4;
    private static final int DEFAULT_NUMBER_OF_CASSE = 50;

    private Chronometer chronometer;

    private boolean showGrid = false;
    private final Paint paintLigne = new Paint();
    private final Paint paintRectTouch = new Paint();
    private final Paint paintRect = new Paint();
    private final Paint paintRectVictoire = new Paint();
    private int numRows = DEFAULT_NUMBER_OF_ROWS, numColumns = DEFAULT_NUMBER_OF_COLUMNS;
    private List<Casse> casses = new ArrayList<Casse>();
    private List<Integer> blacks = new ArrayList<Integer>();
    private Casse casse;
    private boolean isPlay = true;

    public GridLineView(Context context) {
        super(context);
        init();
        toggleGrid();
    }

    public GridLineView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
        toggleGrid();
    }

    public GridLineView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
        toggleGrid();
    }

    private void init() {
        paintLigne.setColor(DEFAULT_PAINT_COLOR_LIGNE);
        paintRect.setColor(DEFAULT_PAINT_COLOR_RECT);
        paintRectTouch.setColor(DEFAULT_PAINT_COLOR_RECT_TOUCH);
        paintRectVictoire.setColor(DEFAULT_PAINT_COLOR_RECT_VICTOIRE);
        initCaseNoir();
    }

    private void initCaseNoir(){
        for (int i=0; i< DEFAULT_NUMBER_OF_CASSE; i++) {
            blacks.add((int) (Math.random() * 4));
        }
    }

    public void setNumberOfRows(int numRows) {
        if (numRows > 0) {
            this.numRows = numRows;
        } else {
            throw new RuntimeException("Cannot have a negative number of rows");
        }
    }

    public void setNumberOfColumns(int numColumns) {
        if (numColumns > 0) {
            this.numColumns = numColumns;
        } else {
            throw new RuntimeException("Cannot have a negative number of columns");
        }
    }

    public void toggleGrid() {
        showGrid = !showGrid;
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        int compte = 4;

        if (showGrid) {
            // drawBlackCasses
            compte = drawBlackCasses(canvas, compte);

            while (compte>0){
                for (int y = 0; y < numColumns; y++){
                    canvas.drawRect(getLeft(y), getTop(compte), getRight(y), getBottom(compte), paintRectVictoire);
                    //Log.e("Vert", "gauche : "+getLeft(y)+", haut : "+getTop(compte)+", droite : " + getRight(y)+", hauteur : " + getBottom(compte));
                }
                compte--;
            }

            drawRedCasses(canvas);
            drawVerticalLines(canvas);
            drawHorizontalLines(canvas);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        super.onTouchEvent(event);
        if(isPlay) {
            startChronometer();
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    if (isToucheLesCasesBlanches(event.getX(), event.getY())) {
                        isPlay = false;
                        casse = new Casse(queuePoisonHoriziontal(event.getX()), queuePoisonVertical(event.getY()), getLongeur(event.getX()), getHauteur(event.getY()));
                        casses.add(casse);
                        invalidate();
                    } else {
                        blacks.remove(0);
                        if(blacks.size() == 0){
                            isPlay = false;
                            stopChronometer();
                        }
                        invalidate();
                    }
                    break;
                case MotionEvent.ACTION_MOVE:
                    break;
                case MotionEvent.ACTION_UP:
                    break;
                case MotionEvent.ACTION_CANCEL:
                    break;
            }
        }
        return true;
    }

    public boolean isGridShown() {
        return showGrid;
    }
    public void setLineColor(int color) {
        paintLigne.setColor(color);
    }
    public void setStrokeWidth(int pixels) {
        paintLigne.setStrokeWidth(pixels);
    }
    public int getNumberOfRows() {
        return numRows;
    }
    public int getNumberOfColumns() {
        return numColumns;
    }

    public void setChronometer(Chronometer chronometer) {
        this.chronometer = chronometer;
    }

    private boolean isToucheLesCasesBlanches(float x, float y) {
        //Log.e("Find", "position X : " +(((int) Math.ceil((x * numColumns) / getMeasuredWidth())) - 1)+ ", BLACK :"+blacks.get(0) + ", position Y :"+((int) Math.ceil((y * numRows) / getMeasuredHeight())));
        return (((int) Math.ceil((x * numColumns) / getMeasuredWidth())) - 1) != blacks.get(0) || ((int) Math.ceil((y * numRows) / getMeasuredHeight())) != numRows;
    }


    private int getHauteur(float y) {
        return (int)(((int) Math.ceil((y * numRows) / getMeasuredHeight()))) *  (getMeasuredHeight() / numRows);
    }

    private int getLongeur(float x) {
        return (int)(((int) Math.ceil((x * numColumns) / getMeasuredWidth()))) *  (getMeasuredWidth() / numColumns);
    }

    private int queuePoisonHoriziontal(float x){
        return (((int) Math.ceil((x * numColumns) / getMeasuredWidth())) - 1) * (getMeasuredWidth() / numColumns);
    }

    private int queuePoisonVertical(float y){
        return (((int) Math.ceil((y * numRows) / getMeasuredHeight())) - 1) * (getMeasuredHeight() / numRows);
    }

    private int getBottom(int compte) {
        return compte * (getMeasuredHeight() / numRows);
    }

    private int getRight(int c) {
        return (c + 1) * (getMeasuredWidth() / numColumns);
    }

    private int getTop(int compte) {
        return (compte - 1) * (getMeasuredHeight() / numRows);
    }

    private int getLeft(int c) {
        return c * (getMeasuredWidth() / numColumns);
    }

    private int drawBlackCasses(Canvas canvas, int compte) {
        for (int i=0; i< blacks.size() && i < numRows; i++) {
            int c = blacks.get(i).intValue();
            canvas.drawRect(getLeft(c), getTop(compte), getRight(c), getBottom(compte), paintRect);
            //Log.e("Interger", "case : "+c+",compte : "+compte);
            //Log.e("Noir", "gauche : "+getLeft(c)+", haut : "+getTop(compte)+", droite : " + getRight(c)+", hauteur : " + getBottom(compte));
            compte--;
        }
        return compte;
    }

    private void drawRedCasses(Canvas canvas) {
        for (int i=0; i< casses.size(); i++) {
            Casse c = casses.get(i);
            canvas.drawRect( c.getVerticale(), c.getHorizontal(), c.getLongueur(), c.getHauteur(), paintRectTouch);
            //Log.e("Rouge", "gauche : "+c.getVerticale()+", haut : "+c.getHorizontal()+", droite : " + c.getLongueur()+", hauteur : " + c.getHauteur());
        }
    }

    private void drawHorizontalLines(Canvas canvas) {
        for (int i = 1; i < numRows; i++) {
            canvas.drawLine(0, getMeasuredHeight() * i / numRows, getMeasuredWidth(), getMeasuredHeight() * i / numRows, paintLigne);
        }
    }

    private void drawVerticalLines(Canvas canvas) {
        for (int i = 1; i < numColumns; i++) {
            canvas.drawLine(getMeasuredWidth() * i / numColumns, 0, getMeasuredWidth() * i / numColumns, getMeasuredHeight(), paintLigne);
        }
    }

    private void startChronometer(){
        chronometer.start();
    }

    private void stopChronometer(){
        chronometer.stop();
    }
}
