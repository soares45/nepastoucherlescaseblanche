package ca.alexsoares.nepastoucherlescaseblanche;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void jouerGame(View view){
        startActivity(new Intent(this, HolderGridLineView.class));
    }

}